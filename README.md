# README #

Solution for Transport salesman problem with using of genetic algorithm.

### How do I get set up? ###

Command line arguments:

* Path to input file
* Size of population
* Number of generation

Sample:

* python tsp.py input.txt 10 100

### Input file format ###

Input file should contain lines with name and coordinates of cities separeted with tabulation. Look for sample in repo.